package com.devcamp.task54b50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task54B50Application {

	public static void main(String[] args) {
		SpringApplication.run(Task54B50Application.class, args);
	}

}
