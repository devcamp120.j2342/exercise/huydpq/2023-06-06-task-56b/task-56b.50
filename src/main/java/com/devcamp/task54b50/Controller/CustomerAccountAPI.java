package com.devcamp.task54b50.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task54b50.Account;
import com.devcamp.task54b50.Customer;

@RestController
public class CustomerAccountAPI {
    @CrossOrigin
    @GetMapping("/account")
    public ArrayList<Account> account() {
        Customer customer1 = new Customer(01, "Hùy", 10);
        Customer customer2 = new Customer(02, "Hùng", 20);
        Customer customer3 = new Customer(03, "Hoa", 30);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

         Account account1= new Account("HD01", customer1, 10000);
          Account account2= new Account("HD02", customer2, 10000);
        Account account3= new Account("HD03", customer3, 10000);
        account1.deposit(5000);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        ArrayList<Account> accounts = new ArrayList<Account>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        return accounts;
    }
}
